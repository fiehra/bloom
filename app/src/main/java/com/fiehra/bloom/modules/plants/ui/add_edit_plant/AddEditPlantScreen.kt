package com.fiehra.bloom.modules.plants.ui.add_edit_plant

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Save
import androidx.compose.material.icons.filled.WaterDrop
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.fiehra.bloom.core.components.PlantExposedSelect
import kotlinx.coroutines.flow.collectLatest
import com.fiehra.bloom.core.components.PlantTextfield

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun AddEditPlantScreen(
  navController: NavController,
  viewModel: AddEditPlantViewModel = hiltViewModel()
) {
  val nameState = viewModel.plantName.value
  val wateringState = viewModel.plantWatering.value
  val lightState = viewModel.plantLight.value
  val thirstyState = viewModel.plantThirsty.value
  val lastWaterState = viewModel.plantLastWater.value
  val lightOptions = listOf("dark", "moderate", "bright")
  val wateringOptions = listOf("1 day", "3 days", "7 days", "14 days", "28 days")
  val snackbarHostState = remember { SnackbarHostState() }
  val scaffoldState = rememberScaffoldState()


  LaunchedEffect(key1 = true) {
    viewModel.eventFlow.collectLatest { event ->
      when (event) {
        is AddEditPlantViewModel.UiEvent.ShowSnackbar -> {
          scaffoldState.snackbarHostState.showSnackbar(
            message = event.message
          )
        }

        is AddEditPlantViewModel.UiEvent.SavePlant -> {
          navController.navigateUp()
        }
      }
    }
  }
  Scaffold(
    floatingActionButtonPosition = FabPosition.End,
    floatingActionButton = {
      FloatingActionButton(
        onClick = {
          viewModel.onEvent(AddEditPlantUIEvent.SavePlant)
        },
        backgroundColor = MaterialTheme.colors.primary
      ) {
        Icon(imageVector = Icons.Default.Save, contentDescription = "save plant")
      }
    },
    scaffoldState = scaffoldState
  ) {
    Column(
      modifier = Modifier
        .fillMaxSize()
        .padding(16.dp),
    ) {
      Row() {
        PlantTextfield(
          text = nameState.text,
          hint = nameState.hint,
          onValueChange = {
            viewModel.onEvent(AddEditPlantUIEvent.EnteredName(it))
          },
          onFocusChange = {
            viewModel.onEvent(AddEditPlantUIEvent.ChangedNameFocus(it))
          },
          isHintVisible = nameState.isHintVisible,
          singleLine = true,
        )
      }
      Spacer(modifier = Modifier.height(10.dp))
      Row() {
        PlantExposedSelect(
          options = wateringOptions,
          optionSelected = wateringState.text,
          label = wateringState.hint,
          onOptionSelected = {
            viewModel.onEvent(AddEditPlantUIEvent.EnteredWatering(it))
          },
          onFocusChange = {
            viewModel.onEvent(AddEditPlantUIEvent.ChangedWateringFocus(it))
          },
        )
      }
      Spacer(modifier = Modifier.height(10.dp))
      Row {
        PlantExposedSelect(
          options = lightOptions,
          optionSelected = lightState.text,
          label = lightState.hint,
          onOptionSelected = {
            viewModel.onEvent(AddEditPlantUIEvent.EnteredLight(it))
          },
          onFocusChange = {
            viewModel.onEvent(AddEditPlantUIEvent.ChangedLightFocus(it))
          },
        )
      }
      Spacer(modifier = Modifier.height(10.dp))
      Row {
        Box(
          modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
          content = {
            if (thirstyState.value) {
              Text(
                color = MaterialTheme.colors.secondary,
                modifier = Modifier.align(Alignment.Center),
                text = "${nameState.text} is thirsty !"
              )
            } else {
              Text(
                color = MaterialTheme.colors.primary,
                modifier = Modifier.align(Alignment.Center),
                text = "last water: " + lastWaterState.text
              )
            }
          }
        )
      }
      Box(
        modifier = Modifier.fillMaxSize()
      ) {
        if (viewModel.plantThirsty.value.value && viewModel.editMode) {
          FloatingActionButton(
            modifier = Modifier.align(Alignment.BottomStart),
            onClick = {
              viewModel.initiateWaterReminder()
            },
            backgroundColor = MaterialTheme.colors.primary
          ) {
            Icon(imageVector = Icons.Default.WaterDrop, contentDescription = "give water")
          }
        }
      }
    }
  }
}
