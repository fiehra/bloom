package com.fiehra.bloom.modules.plants.ui.my_plants

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fiehra.bloom.core.entities.PushTokenEntity
import com.fiehra.bloom.core.data.CreatePlantRequestDTO
import com.fiehra.bloom.core.util.AscDesc
import com.fiehra.bloom.core.util.SortBy
import com.fiehra.bloom.modules.notifications.NotificationUseCases
import com.fiehra.bloom.modules.plants.domain.Plant
import com.fiehra.bloom.modules.plants.domain.PlantUseCases
import com.fiehra.bloom.core.states.MyPlantsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

@HiltViewModel
class PlantsViewModel @Inject constructor(
  private val plantUseCases: PlantUseCases,
  private val notificationUseCases: NotificationUseCases,
) : ViewModel() {
  private val _state = mutableStateOf(MyPlantsState())
  val state: State<MyPlantsState> = _state
  private var recentlyDeletedPlant: Plant? = null
  private var getPlantsJob: Job? = null
  private var token: String = ""

  init {
    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
      if (!task.isSuccessful) {
        Log.e("firstTAG", "Fetching FCM registration token failed", task.exception)
        return@OnCompleteListener
      }
      Log.d("firstTAG", task.result)
      // Get new FCM registration token
      token = task.result
      val pushToken = PushTokenEntity(
        token = token
      )
      viewModelScope.launch {
        notificationUseCases.addToken(pushToken)
      }
    })
    getPlants(SortBy.Name(AscDesc.Ascending), true, "")
  }

  fun onEvent(event: MyPlantsUIEvent) {
    when (event) {
      is MyPlantsUIEvent.Order -> {
        if (state.value.sortPlantsBy::class == event.sortPlants::class &&
          state.value.sortPlantsBy.sortBy == event.sortPlants.sortBy
        ) {
          return
        }
        getPlants(event.sortPlants, false, "")
      }

      is MyPlantsUIEvent.DeletePlant -> {
        viewModelScope.launch {
          plantUseCases.deletePlant(event.plant)
          recentlyDeletedPlant = event.plant
        }
      }

      is MyPlantsUIEvent.RestorePlant -> {
        viewModelScope.launch {
          val plantRequestDTO = CreatePlantRequestDTO(
            name = recentlyDeletedPlant!!.name,
            light = recentlyDeletedPlant!!.light,
            wateringFrequency = recentlyDeletedPlant!!.wateringFrequency,
            thirsty = recentlyDeletedPlant!!.thirsty,
            lastWater = recentlyDeletedPlant!!.lastWater
          )
          plantUseCases.addPlant(plantRequestDTO)
          recentlyDeletedPlant = null
        }
      }

      is MyPlantsUIEvent.ToggleOrderSection -> {
        _state.value = state.value.copy(
          isOrderSectionVisible = !state.value.isOrderSectionVisible
        )
      }
    }
  }

  private fun getPlants(sortPlantsBy: SortBy, fetchFromBackend: Boolean, query: String) {
    getPlantsJob?.cancel()
    getPlantsJob = plantUseCases.getPlants(sortPlantsBy, fetchFromBackend, query)
      .onEach { plants ->
        _state.value = state.value.copy(
          plants = plants,
          sortPlantsBy = sortPlantsBy
        )
      }.launchIn(viewModelScope)
  }
}
