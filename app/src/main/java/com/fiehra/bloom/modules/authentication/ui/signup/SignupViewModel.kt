package com.fiehra.bloom.modules.authentication.ui.signup

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fiehra.bloom.core.data.SignupDTO
import com.fiehra.bloom.core.entities.InvalidSignupException
import com.fiehra.bloom.core.states.TextfieldState
import com.fiehra.bloom.modules.authentication.domain.AuthUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel

class SignupViewModel @Inject constructor(
  savedStateHandle: SavedStateHandle,
  private val authUseCases: AuthUseCases
) : ViewModel() {
  private val _email = mutableStateOf(
    TextfieldState(
      hint = "email"
    )
  )
  private val _username = mutableStateOf(
    TextfieldState(
      hint = "username"
    )
  )
  private val _password = mutableStateOf(
    TextfieldState(
      hint = "password"
    )
  )
  private val _repeatPassword = mutableStateOf(
    TextfieldState(
      hint = "repeat password"
    )
  )

  val email: State<TextfieldState> = _email
  val username: State<TextfieldState> = _username
  val password: State<TextfieldState> = _password
  val repeatPassword: State<TextfieldState> = _repeatPassword

  private val _eventFlow = MutableSharedFlow<UiEvent>()
  val eventFlow = _eventFlow.asSharedFlow()

  fun onEvent(event: SignupUIEvent) {
    when (event) {
      is SignupUIEvent.EnteredEmail -> {
        _email.value = email.value.copy(
          text = event.value
        )
        checkInputValidation()
      }

      is SignupUIEvent.ChangedEmailFocus -> {
        _email.value = email.value.copy(
          isHintVisible = !event.focusState.isFocused && email.value.text.isBlank()

        )
      }

      is SignupUIEvent.EnteredUsername -> {
        _username.value = username.value.copy(
          text = event.value
        )
        checkInputValidation()
      }

      is SignupUIEvent.ChangedUsernameFocus -> {
        _username.value = username.value.copy(
          isHintVisible = !event.focusState.isFocused && username.value.text.isBlank()

        )
      }

      is SignupUIEvent.EnteredPassword -> {
        _password.value = password.value.copy(
          text = event.value
        )
        checkInputValidation()
      }

      is SignupUIEvent.ChangedPasswordFocus -> {
        _password.value = password.value.copy(
          isHintVisible = !event.focusState.isFocused && password.value.text.isBlank()
        )
      }

      is SignupUIEvent.EnteredRepeatPassword -> {
        _repeatPassword.value = repeatPassword.value.copy(
          text = event.value
        )
        checkInputValidation()
      }

      is SignupUIEvent.ChangedRepeatPasswordFocus -> {
        _repeatPassword.value = repeatPassword.value.copy(
          isHintVisible = !event.focusState.isFocused && repeatPassword.value.text.isBlank()
        )
      }

      is SignupUIEvent.Signup -> {
        viewModelScope.launch {
          try {
            val signupDto = SignupDTO(
              email = email.value.text,
              username = username.value.text,
              password = password.value.text
            )
            authUseCases.signup(signupDto)
            _eventFlow.emit(SignupViewModel.UiEvent.ShowSnackbar("welcome to BLOOM!"))
          } catch (e: InvalidSignupException) {
            _eventFlow.emit(
              SignupViewModel.UiEvent.ShowSnackbar(
                message = e.message ?: "signup failed"
              )
            )
          }
        }
      }
    }
  }


  private fun checkInputValidation() {
    val validationResult = authUseCases.validateRegisterInputUseCase(
      email.value.text,
      username.value.text,
      password.value.text,
      repeatPassword.value.text
    )
    Log.d("validation: ", validationResult.toString())
//    processInputValidationType(validationResult)
  }

//  private fun processInputValidationType(input: , type: RegisterInputValidationType){
//    _email.value = when(type){
//      RegisterInputValidationType.EmptyField -> {
//        email.value.copy(text = "empty fields left")
//      }
//      RegisterInputValidationType.NoEmail -> {
//        registerState.copy(errorMessageInput = "No valid email", isInputValid = false)
//      }
//      RegisterInputValidationType.PasswordTooShort -> {
//        registerState.copy(errorMessageInput = "Password too short", isInputValid = false)
//      }
//      RegisterInputValidationType.PasswordsDoNotMatch -> {
//        registerState.copy(errorMessageInput = "Passwords do not match", isInputValid = false)
//      }
//      RegisterInputValidationType.Valid -> {
//        registerState.copy(errorMessageInput = null, isInputValid = true)
//      }
//    }
//  }

  sealed class UiEvent {
    data class ShowSnackbar(val message: String) : UiEvent()
  }


}