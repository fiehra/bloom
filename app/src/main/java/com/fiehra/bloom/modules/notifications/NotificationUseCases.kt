package com.fiehra.bloom.modules.notifications

import com.fiehra.bloom.core.entities.InvalidPlantException
import com.fiehra.bloom.core.data.NotificationDTO
import com.fiehra.bloom.core.entities.PushTokenEntity
import kotlinx.coroutines.flow.Flow

data class NotificationUseCases(
  val addToken: AddToken,
  val getToken: GetToken,
  val sendPush: SendPush,
) {
}

class AddToken(
  private val notificationRepository: NotificationRepositoryI
) {
  @Throws(InvalidPlantException::class)
  suspend operator fun invoke(token: PushTokenEntity) {
    notificationRepository.insertPushNotificationToken(token)
  }
}

class GetToken
  (
  private val notificationRepository: NotificationRepositoryI,
) {
  operator fun invoke(
  ): Flow<PushTokenEntity> {
    return notificationRepository.getPushNotificationToken()
  }
}

class SendPush(
  private val notificationRepository: NotificationRepositoryI
) {
  suspend operator fun invoke(notification: NotificationDTO) {
    notificationRepository.sendWaterReminderPushNotification(notification)
  }
}

