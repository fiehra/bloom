package com.fiehra.bloom.modules.plants.ui.add_edit_plant

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fiehra.bloom.core.entities.InvalidPlantException
import com.fiehra.bloom.core.data.CreatePlantRequestDTO
import com.fiehra.bloom.core.data.NotificationDTO
import com.fiehra.bloom.modules.notifications.NotificationUseCases
import com.fiehra.bloom.modules.plants.domain.Plant
import com.fiehra.bloom.core.states.BooleanState
import com.fiehra.bloom.modules.plants.domain.PlantRepositoryI
import com.fiehra.bloom.core.states.SelectState
import com.fiehra.bloom.core.states.TextfieldState
import com.fiehra.bloom.modules.plants.domain.PlantUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class AddEditPlantViewModel @Inject constructor(
  savedStateHandle: SavedStateHandle,
  private val plantUseCases: PlantUseCases,
  private val notificationUseCases: NotificationUseCases,
  private val plantRepository: PlantRepositoryI,
) : ViewModel() {
  private var token: String = ""
  var editMode: Boolean = false

  private val _plantName = mutableStateOf(
    TextfieldState(
      hint = "name"
    )
  )
  private val _plantWatering = mutableStateOf(
    TextfieldState(
      hint = "watering frequency"
    )
  )
  private val _plantLight = mutableStateOf(
    SelectState(
      hint = "light"
    )
  )
  private val _plantThirsty = mutableStateOf(
    BooleanState(
      true
    )
  )
  private val _plantLastWater = mutableStateOf(
    TextfieldState(
      ""
    )
  )

  val plantName: State<TextfieldState> = _plantName
  val plantWatering: State<TextfieldState> = _plantWatering
  val plantLight: State<SelectState> = _plantLight
  val plantThirsty: State<BooleanState> = _plantThirsty
  val plantLastWater: State<TextfieldState> = _plantLastWater

  private val _eventFlow = MutableSharedFlow<UiEvent>()
  val eventFlow = _eventFlow.asSharedFlow()
  private var currentPlantId: String = ""

  init {
    savedStateHandle.get<String>("plantId")?.let { plantId ->
      if (plantId != "") {
        editMode = true
        viewModelScope.launch {
          plantRepository.getPlantById(plantId)?.also { plant ->
            currentPlantId = plant._id
            _plantName.value = plantName.value.copy(
              text = plant.name,
              isHintVisible = false
            )
            _plantWatering.value = plantWatering.value.copy(
              text = plant.wateringFrequency,
              isHintVisible = false
            )
            _plantLight.value = plantLight.value.copy(
              text = plant.light,
              isHintVisible = false
            )
            _plantThirsty.value = plantThirsty.value.copy(
              value = plant.thirsty,
            )
            _plantLastWater.value = plantLastWater.value.copy(
              text = plant.lastWater,
            )
          }
          notificationUseCases.getToken().collect {
            token = it.token
          }
        }
      }
    }
  }

  fun onEvent(event: AddEditPlantUIEvent) {
    when (event) {
      is AddEditPlantUIEvent.EnteredName -> {
        _plantName.value = plantName.value.copy(
          text = event.value
        )
      }

      is AddEditPlantUIEvent.ChangedNameFocus -> {
        _plantName.value = plantName.value.copy(
          isHintVisible = !event.focusState.isFocused && plantName.value.text.isBlank()
        )
      }

      is AddEditPlantUIEvent.EnteredWatering -> {
        _plantWatering.value = plantWatering.value.copy(
          text = event.value,
        )
      }

      is AddEditPlantUIEvent.ChangedWateringFocus -> {
        _plantWatering.value = plantWatering.value.copy(
          isHintVisible = !event.focusState.isFocused && plantWatering.value.text.isBlank()
        )
      }

      is AddEditPlantUIEvent.EnteredLight -> {
        _plantLight.value = plantLight.value.copy(
          text = event.value
        )
      }

      is AddEditPlantUIEvent.ChangedLightFocus -> {
        _plantLight.value = plantLight.value.copy(
          isHintVisible = !event.focusState.isFocused && plantLight.value.text.isBlank()
        )
      }

      is AddEditPlantUIEvent.SavePlant -> {
        if (editMode) {
          updatePlant()
          viewModelScope.launch {
            _eventFlow.emit(UiEvent.SavePlant)
          }
        } else {
          viewModelScope.launch {
            try {
              val plant = CreatePlantRequestDTO(
                name = plantName.value.text,
                wateringFrequency = plantWatering.value.text,
                light = plantLight.value.text,
                thirsty = plantThirsty.value.value,
                lastWater = plantLastWater.value.text
              )
              plantUseCases.addPlant(plant)
              _eventFlow.emit(UiEvent.SavePlant)
            } catch (e: InvalidPlantException) {
              _eventFlow.emit(
                UiEvent.ShowSnackbar(
                  message = e.message ?: "adding plant failed"
                )
              )
            }
          }
        }
      }
    }
  }

  fun initiateWaterReminder() {
    val now = LocalDate.now().toString()
    val notification = NotificationDTO(
      plantId = currentPlantId,
      registrationToken = token,
      notificationTitle = "${_plantName.value.text} is thirsty!",
      notificationBody = "please water your plant",
      wateringFrequency = _plantWatering.value.text,
      lastWater = now
    )
    _plantThirsty.value = plantThirsty.value.copy(
      value = false
    )
    _plantLastWater.value = plantLastWater.value.copy(
      text = now
    )

    viewModelScope.launch {
      notificationUseCases.sendPush(notification)
    }
    updatePlant()
  }

  private fun updatePlant() {
    val plant = Plant(
      _id = currentPlantId,
      name = plantName.value.text,
      wateringFrequency = plantWatering.value.text,
      light = plantLight.value.text,
      thirsty = plantThirsty.value.value,
      lastWater = plantLastWater.value.text
    )
    viewModelScope.launch {
      try {
        plantUseCases.updatePlant(plant)
//        _eventFlow.emit(UiEvent.SavePlant)
      } catch (e: InvalidPlantException) {
        _eventFlow.emit(
          UiEvent.ShowSnackbar(
            message = e.message ?: "saving plant failed"
          )
        )
      }
    }
  }

  sealed class UiEvent {
    data class ShowSnackbar(val message: String) : UiEvent()
    object SavePlant : UiEvent()
  }
}