package com.fiehra.bloom.modules.plants.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Plant(
  @SerialName("_id") val _id: String,
  val name: String,
  val wateringFrequency: String,
  val light: String,
  val thirsty: Boolean,
  val lastWater: String
) {}