package com.fiehra.bloom.modules.authentication.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.text.DateFormat
import java.util.Date


@Serializable
data class AuthModel(
  @SerialName("_id") val _id: String,
  val userId: String,
  val email: String,
  val username: String,
  val role: String,
  val token: String,
  val expirationDate: Float,
) {}
