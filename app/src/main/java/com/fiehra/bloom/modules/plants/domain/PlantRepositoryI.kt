package com.fiehra.bloom.modules.plants.domain

import com.fiehra.bloom.core.data.CreatePlantRequestDTO
import kotlinx.coroutines.flow.Flow

interface PlantRepositoryI {

  fun getPlants(
    fetchFromBackend: Boolean,
    query: String,
  ): Flow<List<Plant>>

  suspend fun getPlantById(id: String): Plant?

  suspend fun insertPlant(plant: CreatePlantRequestDTO)

  suspend fun updatePlant(plant: Plant)

  suspend fun deletePlant(plant: Plant)
}