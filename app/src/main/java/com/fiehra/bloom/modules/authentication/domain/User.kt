package com.fiehra.bloom.modules.authentication.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class User(
  @SerialName("_id") val _id: String,
  val email: String,
  val username: String,
  val password: String,
  val role: String,
) {}