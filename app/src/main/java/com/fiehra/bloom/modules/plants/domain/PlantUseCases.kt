package com.fiehra.bloom.modules.plants.domain

import com.fiehra.bloom.core.data.CreatePlantRequestDTO
import com.fiehra.bloom.core.entities.InvalidPlantException
import com.fiehra.bloom.core.util.SortBy
import com.fiehra.bloom.core.util.AscDesc
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

data class PlantUseCases(
  val getPlants: GetPlants,
//  val getPlant: GetPlantById,
  val addPlant: AddPlant,
  val deletePlant: DeletePlant,
  val updatePlant: UpdatePlant,
) {
}

class AddPlant(
  private val plantRepository: PlantRepositoryI
) {
  @Throws(InvalidPlantException::class)
  suspend operator fun invoke(plant: CreatePlantRequestDTO) {
    if (plant.name.isBlank()) {
      throw InvalidPlantException("name cant be empty")
    }
    if (plant.light.isBlank()) {
      throw InvalidPlantException("light cant be empty")
    }
    if (plant.wateringFrequency.isBlank()) {
      throw InvalidPlantException("watering frequency cant be empty")
    }
    plantRepository.insertPlant(plant)
  }
}

class DeletePlant(
  private val plantRepository: PlantRepositoryI
) {
  suspend operator fun invoke(plant: Plant) {
    plantRepository.deletePlant(plant)
  }
}


class GetPlants
  (
  private val plantRepository: PlantRepositoryI,
) {
  operator fun invoke(
    sortPlantsBy: SortBy = SortBy.Name(AscDesc.Descending),
    fetchFromBackend: Boolean,
    query: String
  ): Flow<List<Plant>> {
    return plantRepository.getPlants(fetchFromBackend, query).map { plants ->
      when (sortPlantsBy.sortBy) {
        is AscDesc.Ascending -> {
          when (sortPlantsBy) {
            is SortBy.Name -> plants.sortedBy { it.name.lowercase() }
            is SortBy.WateringFrequency -> plants.sortedBy { it.wateringFrequency }
            is SortBy.Light -> plants.sortedBy { it.light.lowercase() }
          }
        }

        is AscDesc.Descending -> {
          when (sortPlantsBy) {
            is SortBy.Name -> plants.sortedByDescending { it.name.lowercase() }
            is SortBy.WateringFrequency -> plants.sortedByDescending { it.wateringFrequency }
            is SortBy.Light -> plants.sortedByDescending { it.light.lowercase() }
          }
        }
      }
    }
  }
}


class UpdatePlant(
  private val repository: PlantRepositoryI
) {
  @Throws(InvalidPlantException::class)
  suspend operator fun invoke(plant: Plant) {
    if (plant.name.isBlank()) {
      throw InvalidPlantException("the name cant be empty")
    }
    if (plant.light.isBlank()) {
      throw InvalidPlantException("the light cant be empty")
    }
    repository.updatePlant(plant)
  }
}

class GetPlantById
  (
  private val plantRepository: PlantRepositoryI
) {
  suspend operator fun invoke(id: String): Plant? {
    return plantRepository.getPlantById(id)
  }
}

