package com.fiehra.bloom.modules.authentication.domain

//import com.fiehra.bloom.core.data.AuthApiI
import com.fiehra.bloom.core.data.LoginDTO
import com.fiehra.bloom.core.data.SignupDTO
import com.fiehra.bloom.core.entities.InvalidSignupException

enum class RegisterInputValidationType {
  EmptyField,
  NoEmail,
  PasswordsDoNotMatch,
  PasswordTooShort,
  Valid
}


data class AuthUseCases(
  val signup: Signup,
  val login: Login,
  val validateRegisterInputUseCase: ValidateRegisterInputUseCase,
) {
}

class Signup(
//  private val authApi: AuthApiI
) {
  @Throws(InvalidSignupException::class)
  suspend operator fun invoke(signupDTO: SignupDTO) {
    if (signupDTO.email.isBlank()) {
      throw InvalidSignupException("email cant be empty")
    }
    if (signupDTO.username.isBlank()) {
      throw InvalidSignupException("username cant be empty")
    }
    if (signupDTO.password.isBlank()) {
      throw InvalidSignupException("password cant be empty")
    }
//    authApi.signup(signupDTO)
  }
}

class ValidateRegisterInputUseCase {
  operator fun invoke(
    email: String,
    username: String,
    password: String,
    passwordRepeated: String
  ): RegisterInputValidationType {
    if (username.isEmpty() || email.isEmpty() || password.isEmpty() || passwordRepeated.isEmpty()) {
      return RegisterInputValidationType.EmptyField
    }
    if ("@" !in email) {
      return RegisterInputValidationType.NoEmail
    }
    if (password != passwordRepeated) {
      return RegisterInputValidationType.PasswordsDoNotMatch
    }
    if (password.count() < 8) {
      return RegisterInputValidationType.PasswordTooShort
    }
    return RegisterInputValidationType.Valid
  }
}

class Login(
//  private val authRepository: AuthRepositoryI
) {
  suspend operator fun invoke(loginDTO: LoginDTO) {
//    authRepository.login(loginDTO)
  }
}



