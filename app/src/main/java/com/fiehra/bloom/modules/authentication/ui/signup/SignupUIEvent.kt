package com.fiehra.bloom.modules.authentication.ui.signup

import androidx.compose.ui.focus.FocusState
import com.fiehra.bloom.modules.plants.ui.add_edit_plant.AddEditPlantUIEvent

sealed class SignupUIEvent {
  data class EnteredEmail(val value: String) : SignupUIEvent()
  data class ChangedEmailFocus(val focusState: FocusState) : SignupUIEvent()
  data class EnteredUsername(val value: String) : SignupUIEvent()
  data class ChangedUsernameFocus(val focusState: FocusState) : SignupUIEvent()
  data class EnteredPassword(val value: String) : SignupUIEvent()
  data class ChangedPasswordFocus(val focusState: FocusState) : SignupUIEvent()
  data class EnteredRepeatPassword(val value: String) : SignupUIEvent()
  data class ChangedRepeatPasswordFocus(val focusState: FocusState) : SignupUIEvent()

  object Signup : SignupUIEvent()
}
