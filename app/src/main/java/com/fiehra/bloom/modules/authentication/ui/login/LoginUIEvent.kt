package com.fiehra.bloom.modules.authentication.ui.login

import androidx.compose.ui.focus.FocusState

sealed class LoginUIEvent {
  data class EnteredUserName(val value: String) : LoginUIEvent()
  data class ChangedUserNameFocus(val focusState: FocusState) : LoginUIEvent()
  data class EnteredPassword(val value: String) : LoginUIEvent()
  data class ChangedPasswordFocus(val focusState: FocusState) : LoginUIEvent()
}
