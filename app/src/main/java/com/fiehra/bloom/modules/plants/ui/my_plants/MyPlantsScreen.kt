package com.fiehra.bloom.modules.plants.ui.my_plants


import android.annotation.SuppressLint
import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Sort
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.icons.filled.Alarm
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.fiehra.bloom.core.components.OrderSection
import com.fiehra.bloom.core.components.PlantItem
import com.fiehra.bloom.core.util.Screen
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MyPlantsScreen(
  navController: NavController,
  viewModel: PlantsViewModel = hiltViewModel(),
) {
  val state = viewModel.state.value
  val snackbarHostState = remember { SnackbarHostState() }
  val scaffoldState = rememberScaffoldState()
  val scope = rememberCoroutineScope()

  Scaffold(
    floatingActionButton = {
      FloatingActionButton(
        onClick = {
          navController.navigate(Screen.AddEditPlantScreen.route)
        },
        backgroundColor = MaterialTheme.colors.primary
      ) {
        Icon(imageVector = Icons.Default.Add, contentDescription = "add")
      }

    },
    scaffoldState = scaffoldState,
    snackbarHost = {
      SnackbarHost(it) { data ->
        Snackbar(
          backgroundColor = MaterialTheme.colors.primary,
          actionColor = MaterialTheme.colors.surface,
          snackbarData = data
        )
      }
    },
  ) {
    Column(
      modifier = Modifier
        .fillMaxSize()
        .padding(10.dp)
    ) {
      Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
      ) {
        Text(
          text = "BLOOM",
          color = MaterialTheme.colors.primary
        )
        IconButton(onClick = { viewModel.onEvent(MyPlantsUIEvent.ToggleOrderSection) }) {
          Icon(
            imageVector = Icons.Default.Sort,
            contentDescription = "sort",
            tint = if (state.isOrderSectionVisible) {
              MaterialTheme.colors.primary
            } else {
              MaterialTheme.colors.onBackground
            }
          )
        }
        IconButton(onClick = { navController.navigate(Screen.SignupScreen.route) }) {
          Icon(
            imageVector = Icons.Default.Alarm,
            contentDescription = "alarm",
            tint =
            MaterialTheme.colors.primary
          )
        }
      }
      AnimatedVisibility(
        visible = state.isOrderSectionVisible,
        enter = fadeIn() + slideInVertically(),
        exit = fadeOut() + slideOutVertically()
      ) {
        OrderSection(
          modifier = Modifier
            .fillMaxWidth(),
          sortBy = state.sortPlantsBy,
          onOrderChange = {
            viewModel.onEvent(MyPlantsUIEvent.Order(it))
          }
        )
      }
      Spacer(modifier = Modifier.height(16.dp))
      LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(state.plants) { plant ->
          PlantItem(
            plant = plant,
            modifier = Modifier
              .fillMaxWidth()
              .clickable {
                navController.navigate(
                  Screen.AddEditPlantScreen.route + "?plantId=${plant._id}"
                )
              },
            onDeleteClick = {
              viewModel.onEvent(MyPlantsUIEvent.DeletePlant(plant))
              scope.launch {
                val result = scaffoldState.snackbarHostState.showSnackbar(
                  message = "plant deleted",
                  actionLabel = "undo",
                  duration = SnackbarDuration.Short,
                )
                if (result == SnackbarResult.ActionPerformed) {
                  viewModel.onEvent(MyPlantsUIEvent.RestorePlant)
                }
              }
            }
          )
          Spacer(modifier = Modifier.height(10.dp))
        }
      }
    }
  }
}