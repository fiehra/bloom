package com.fiehra.bloom.modules.plants.ui.my_plants

import com.fiehra.bloom.modules.plants.domain.Plant
import com.fiehra.bloom.core.util.SortBy

sealed class MyPlantsUIEvent {
  data class Order(val sortPlants: SortBy) : MyPlantsUIEvent()
  data class DeletePlant(val plant: Plant) : MyPlantsUIEvent()
  object RestorePlant : MyPlantsUIEvent()
  object ToggleOrderSection : MyPlantsUIEvent()
}

