package com.fiehra.bloom.modules.plants.ui.add_edit_plant

import androidx.compose.ui.focus.FocusState

sealed class AddEditPlantUIEvent {
  data class EnteredName(val value: String) : AddEditPlantUIEvent()
  data class ChangedNameFocus(val focusState: FocusState) : AddEditPlantUIEvent()
  data class EnteredWatering(val value: String) : AddEditPlantUIEvent()
  data class ChangedWateringFocus(val focusState: FocusState) : AddEditPlantUIEvent()
  data class EnteredLight(val value: String) : AddEditPlantUIEvent()
  data class ChangedLightFocus(val focusState: FocusState) : AddEditPlantUIEvent()

  object SavePlant : AddEditPlantUIEvent()
}
