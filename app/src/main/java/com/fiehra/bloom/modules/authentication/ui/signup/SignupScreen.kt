package com.fiehra.bloom.modules.plants.ui.my_plants


import android.annotation.SuppressLint
import androidx.compose.animation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Save
import androidx.compose.material.icons.filled.Sort
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.fiehra.bloom.core.components.PlantTextfield
import com.fiehra.bloom.core.util.Screen
import com.fiehra.bloom.modules.authentication.ui.signup.SignupViewModel
import com.fiehra.bloom.modules.authentication.ui.signup.SignupUIEvent

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SignupScreen(
  navController: NavController,
  viewModel: SignupViewModel = hiltViewModel(),
) {
  val scaffoldState = rememberScaffoldState()

  val emailState = viewModel.email.value
  val usernameState = viewModel.username.value
  val passwordState = viewModel.password.value
  val repeatPasswordState = viewModel.repeatPassword.value

  Scaffold(
//    floatingActionButtonPosition = FabPosition.End,
//    floatingActionButton = {
//      FloatingActionButton(
//        onClick = {
//          viewModel.onEvent(SignupUIEvent.Signup)
//        },
//        backgroundColor = MaterialTheme.colors.primary
//      ) {
//        Icon(imageVector = Icons.Default.Save, contentDescription = "save plant")
//      }
//    },
    scaffoldState = scaffoldState
  ) {
    Column(
      modifier = Modifier
        .fillMaxSize()
        .padding(16.dp),
      verticalArrangement = Arrangement.Top
    ) {
      Spacer(modifier = Modifier.height(10.dp))
      Text(
        modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center,
        text = "SIGNUP",
        color = MaterialTheme.colors.primary
      )
      Spacer(modifier = Modifier.height(10.dp))
      PlantTextfield(
        text = emailState.text,
        hint = emailState.hint,
        onValueChange = {
          viewModel.onEvent(SignupUIEvent.EnteredEmail(it))
        },
        onFocusChange = {
          viewModel.onEvent(SignupUIEvent.ChangedEmailFocus(it))
        },
        isHintVisible = emailState.isHintVisible,
        singleLine = true,
      )
      Spacer(modifier = Modifier.height(10.dp))
      PlantTextfield(
        text = usernameState.text,
        hint = usernameState.hint,
        onValueChange = {
          viewModel.onEvent(SignupUIEvent.EnteredUsername(it))
        },
        onFocusChange = {
          viewModel.onEvent(SignupUIEvent.ChangedUsernameFocus(it))
        },
        isHintVisible = usernameState.isHintVisible,
        singleLine = true,
      )
      Spacer(modifier = Modifier.height(10.dp))
      PlantTextfield(
        text = passwordState.text,
        hint = passwordState.hint,
        onValueChange = {
          viewModel.onEvent(SignupUIEvent.EnteredPassword(it))
        },
        onFocusChange = {
          viewModel.onEvent(SignupUIEvent.ChangedPasswordFocus(it))
        },
        isHintVisible = passwordState.isHintVisible,
        singleLine = true,
      )
      Spacer(modifier = Modifier.height(10.dp))
      PlantTextfield(
        text = repeatPasswordState.text,
        hint = repeatPasswordState.hint,
        onValueChange = {
          viewModel.onEvent(SignupUIEvent.EnteredRepeatPassword(it))
        },
        onFocusChange = {
          viewModel.onEvent(SignupUIEvent.ChangedRepeatPasswordFocus(it))
        },
        isHintVisible = repeatPasswordState.isHintVisible,
        singleLine = true,
      )
      Spacer(modifier = Modifier.height(40.dp))
      IconButton(onClick = { navController.navigate(Screen.MyPlantsScreen.route) }) {
        Icon(
          imageVector = Icons.Default.Home,
          contentDescription = "home",
          tint = MaterialTheme.colors.primary
        )
      }
    }
  }
}