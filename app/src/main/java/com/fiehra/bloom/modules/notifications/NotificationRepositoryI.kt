package com.fiehra.bloom.modules.notifications

import com.fiehra.bloom.core.entities.PushTokenEntity
import com.fiehra.bloom.core.data.NotificationDTO
import kotlinx.coroutines.flow.Flow

interface NotificationRepositoryI {

  fun getPushNotificationToken(): Flow<PushTokenEntity>

  suspend fun insertPushNotificationToken(token: PushTokenEntity)

  suspend fun clearToken()

  suspend fun sendWaterReminderPushNotification(notification: NotificationDTO)
}