package com.fiehra.bloom

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BloomApp : Application() {
  override fun onCreate() {
    super.onCreate()
  }
}


