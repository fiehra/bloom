package com.fiehra.bloom.core.data

import kotlinx.serialization.Serializable

@Serializable
data class HttpResponseMessageDTO(
  val message: String,
)

@Serializable
data class HttpResponseSignupDTO(
  val message: String,
  val userId: String,
)
