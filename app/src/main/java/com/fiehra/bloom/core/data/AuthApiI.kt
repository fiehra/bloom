package com.fiehra.bloom.core.data

import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*

interface AuthApiI {
  suspend fun signup(signupDTO: SignupDTO): HttpResponseSignupDTO?

  suspend fun login(loginDTO: LoginDTO): HttpResponseMessageDTO?

  companion object {
    fun create(): AuthApi {
      return AuthApi(client = HttpClient(Android) {
        install(JsonFeature) {
          serializer = KotlinxSerializer()
        }
      })
    }
  }
}