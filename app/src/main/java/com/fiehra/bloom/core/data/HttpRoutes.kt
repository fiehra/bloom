package com.fiehra.bloom.core.data

object HttpRoutes {
  // realworld
  //  private const val backendUrl = "https://api.nyanya.cat/api"

  // samsung make sure youre connected with same wifi
  private const val backendUrl = "http://192.168.178.24:2507/api"

  // emulator
  //  private const val backendUrl = "http://10.0.2.2:2507/api"
  const val getAllPlantsRoute = "$backendUrl/plants/getAll"
  const val getPlantByIdRoute = "$backendUrl/plants/getById"
  const val createPlantRoute = "$backendUrl/plants/create"
  const val updatePlantRoute = "$backendUrl/plants/update"
  const val deletePlantRoute = "$backendUrl/plants/delete"

  const val signupRoute = "$backendUrl/authentication/signup"
  const val loginRoute = "$backendUrl/authentication/login"

  const val sendWaterReminderRoute = "$backendUrl/notifications/sendWaterReminder"
}
