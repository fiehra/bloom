package com.fiehra.bloom.core.data

import kotlinx.serialization.Serializable

@Serializable
data class LoginDTO(
  val username: String,
  val password: String,
)

@Serializable
data class SignupDTO(
  val email: String,
  val username: String,
  val password: String,
)

