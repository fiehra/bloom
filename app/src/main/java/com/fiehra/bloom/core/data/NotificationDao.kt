package com.fiehra.bloom.core.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fiehra.bloom.core.entities.PushTokenEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotificationDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insertPushNotificationToken(
    token: PushTokenEntity
  )

  @Query("DELETE FROM PushTokenEntity")
  suspend fun clearToken()

  @Query(
    "SELECT * FROM PushTokenEntity"
  )
  fun getPushNotificationToken(): Flow<PushTokenEntity>
}