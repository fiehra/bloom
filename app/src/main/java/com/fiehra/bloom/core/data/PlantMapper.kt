package com.fiehra.bloom.core.data

import com.fiehra.bloom.core.entities.PlantEntity
import com.fiehra.bloom.modules.plants.domain.Plant

fun PlantEntity.toPlant(): Plant {
  return Plant(
    _id = id,
    name = name,
    wateringFrequency = wateringFrequency,
    light = light,
    thirsty = thirsty,
    lastWater = lastWater
  )
}

fun Plant.toPlantEntity(): PlantEntity {
  return PlantEntity(
    id = _id,
    name = name,
    wateringFrequency = wateringFrequency,
    light = light,
    thirsty = thirsty,
    lastWater = lastWater
  )
}