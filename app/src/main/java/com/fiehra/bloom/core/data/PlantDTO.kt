package com.fiehra.bloom.core.data

import com.fiehra.bloom.modules.plants.domain.Plant
import kotlinx.serialization.Serializable

@Serializable
data class CreatePlantRequestDTO(
  val name: String,
  val wateringFrequency: String,
  val light: String,
  val thirsty: Boolean,
  val lastWater: String
) {
}

@Serializable
data class GetPlantsResponseDTO(
  val plants: List<Plant>,
  val message: String,
  val maxPlants: Long
)

@Serializable
data class CreateUpdatePlantResponseDTO(
  val message: String,
  val plantId: String
)

