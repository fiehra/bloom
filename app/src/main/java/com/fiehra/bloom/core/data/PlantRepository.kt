package com.fiehra.bloom.core.data

import com.fiehra.bloom.core.entities.PlantEntity
import com.fiehra.bloom.modules.plants.domain.Plant
import com.fiehra.bloom.modules.plants.domain.PlantRepositoryI
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlantRepository @Inject constructor(
  db: PlantDatabase
) : PlantRepositoryI {
  private val plantApi = PlantApiI.create()
  private val dao = db.plantDao

  override fun getPlants(
    fetchFromBackend: Boolean,
    query: String
  ): Flow<List<Plant>> {
    return flow {
      if (fetchFromBackend) {
        val response = plantApi.getPlants().plants
        dao.clearPlants()
        dao.insertPlants(
          response.map { it.toPlantEntity() }
        )
        val localPlants = dao.searchPlants(query)
        localPlants.collect { plants ->
          emit(plants.map { it.toPlant() })
          return@collect
        }
      } else {
        val localPlants = dao.searchPlants(query)
        localPlants.collect { plants ->
          emit(plants.map { it.toPlant() })
          return@collect
        }
      }
    }
  }

  override suspend fun getPlantById(id: String): Plant {
    return dao.getPlantById(id).toPlant()
  }

  override suspend fun updatePlant(plant: Plant) {
    dao.insertPlant(plant.toPlantEntity())
    dao.searchPlants("")
    plantApi.updatePlant(plant)
  }

  override suspend fun insertPlant(plant: CreatePlantRequestDTO) {
    val response = plantApi.createPlant(plant)
    val id = response?.plantId
    if (id != null) {
      val plantEntity = PlantEntity(
        id = id,
        name = plant.name,
        wateringFrequency = plant.wateringFrequency,
        light = plant.light,
        thirsty = plant.thirsty,
        lastWater = plant.lastWater
      )
      dao.insertPlant(plantEntity)
      dao.searchPlants("")
    }
  }

  override suspend fun deletePlant(plant: Plant) {
    dao.deletePlant(plant.toPlantEntity())
    plantApi.deletePlant(plant._id)
  }
}
