package com.fiehra.bloom.core.data

import android.util.Log
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*

class NotificationApi(
  private val client: HttpClient
) : NotificationApiI {
  override suspend fun sendWaterReminderPushNotification(notification: NotificationDTO): HttpResponseMessageDTO? {
    return try {
      client.post {
        Log.d("req.body", notification.registrationToken)
        Log.d("req.body", notification.notificationBody)
        Log.d("req.body", notification.notificationTitle)
        Log.d("req.body", notification.wateringFrequency)
        Log.d("req.body", notification.lastWater)
        url(HttpRoutes.sendWaterReminderRoute)
        contentType(ContentType.Application.Json)
        body = notification
      }
    } catch (error: Exception) {
      return null
    }
  }
}