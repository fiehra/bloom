package com.fiehra.bloom.core.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PlantEntity(
  @PrimaryKey val id: String,
  val name: String,
  val wateringFrequency: String,
  val light: String,
  val thirsty: Boolean,
  val lastWater: String
)

class InvalidPlantException(message: String) : Exception(message)
class InvalidLoginException(message: String) : Exception(message)
class InvalidSignupException(message: String) : Exception(message)