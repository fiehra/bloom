package com.fiehra.bloom.core.data


import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.lang.Exception

class AuthApi(
  private val client: HttpClient
) : AuthApiI {
  override suspend fun signup(signupDTO: SignupDTO): HttpResponseSignupDTO? {
    return try {
      client.post {
        url(HttpRoutes.signupRoute)
        contentType(ContentType.Application.Json)
        body = signupDTO
      }

    } catch (error: Exception) {
      return null
    }
  }

  override suspend fun login(loginDTO: LoginDTO): HttpResponseMessageDTO? {
    return try {
      client.post {
        url(HttpRoutes.loginRoute)
        contentType(ContentType.Application.Json)
        body = loginDTO
      }

    } catch (error: Exception) {
      return null
    }
  }
}

