package com.fiehra.bloom.core.data

import android.util.Log
import com.fiehra.bloom.modules.plants.domain.Plant
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.lang.Exception

class PlantApi(
  private val client: HttpClient
) : PlantApiI {
  override suspend fun getPlants(): GetPlantsResponseDTO {
    return try {
      client.get {
        url(HttpRoutes.getAllPlantsRoute)
      }
    } catch (error: Exception) {
      Log.e("error", "$error")
      return GetPlantsResponseDTO(plants = emptyList(), message = "error", maxPlants = 0)
    }
  }

  //  override suspend fun getPlantById(id: Int): PlantResponseDto {
//    TODO("Not yet implemented")
//  }
//
  override suspend fun createPlant(plantRequest: CreatePlantRequestDTO): CreateUpdatePlantResponseDTO? {
    return try {
      client.post {
        url(HttpRoutes.createPlantRoute)
        contentType(ContentType.Application.Json)
        body = plantRequest
      }

    } catch (error: Exception) {
      return null
    }
  }

  override suspend fun updatePlant(plant: Plant): HttpResponseMessageDTO? {
    return try {
      client.put {
        url(HttpRoutes.updatePlantRoute + "/" + plant._id)
        contentType(ContentType.Application.Json)
        body = plant
      }

    } catch (error: Exception) {
      return null
    }
  }

  override suspend fun deletePlant(plantId: String): HttpResponseMessageDTO? {
    return try {
      client.delete {
        url(HttpRoutes.deletePlantRoute + "/" + plantId)
        contentType(ContentType.Application.Json)
      }
    } catch (error: Exception) {
      return null
    }
  }
}