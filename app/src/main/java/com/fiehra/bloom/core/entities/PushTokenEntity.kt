package com.fiehra.bloom.core.entities


import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PushTokenEntity(
  @PrimaryKey val token: String,
)
