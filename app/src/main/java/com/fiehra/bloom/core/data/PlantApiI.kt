package com.fiehra.bloom.core.data

import com.fiehra.bloom.modules.plants.domain.Plant
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*

interface PlantApiI {
  suspend fun getPlants(): GetPlantsResponseDTO

  suspend fun createPlant(plantRequest: CreatePlantRequestDTO): CreateUpdatePlantResponseDTO?

  suspend fun updatePlant(plant: Plant): HttpResponseMessageDTO?

  suspend fun deletePlant(plantId: String): HttpResponseMessageDTO?

  companion object {
    fun create(): PlantApi {
      return PlantApi(client = HttpClient(Android) {
        install(JsonFeature) {
          serializer = KotlinxSerializer()
        }
      })
    }
  }
}