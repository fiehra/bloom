package com.fiehra.bloom.core.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.fiehra.bloom.core.entities.PlantEntity
import com.fiehra.bloom.core.entities.PushTokenEntity

@Database(
  entities = [PlantEntity::class, PushTokenEntity::class],
  version = 2
)

abstract class PlantDatabase : RoomDatabase() {
  abstract val plantDao: PlantDao
  abstract val pushNotificationDao: NotificationDao
}

