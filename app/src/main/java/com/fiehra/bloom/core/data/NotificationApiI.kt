package com.fiehra.bloom.core.data

import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*

interface NotificationApiI {
  suspend fun sendWaterReminderPushNotification(notification: NotificationDTO): HttpResponseMessageDTO?

  companion object {
    fun create(): NotificationApiI {
      return NotificationApi(client = HttpClient(Android) {
        install(JsonFeature) {
          serializer = KotlinxSerializer()
        }
      })
    }
  }

}