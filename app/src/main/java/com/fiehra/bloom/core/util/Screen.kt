package com.fiehra.bloom.core.util

sealed class Screen(val route: String) {
  object MyPlantsScreen : Screen("my_plants_screen")
  object SignupScreen : Screen("signup_screen")
  object LoginScreen : Screen("login_screen")
  object AddEditPlantScreen : Screen("add_edit_plant_screen")
}
