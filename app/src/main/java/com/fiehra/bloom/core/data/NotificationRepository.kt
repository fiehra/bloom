package com.fiehra.bloom.core.data

import com.fiehra.bloom.core.entities.PushTokenEntity
import com.fiehra.bloom.modules.notifications.NotificationRepositoryI
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class NotificationRepository @Inject constructor(
  db: PlantDatabase
) : NotificationRepositoryI {
  private val pushNotificationApi = NotificationApiI.create()
  private val dao = db.pushNotificationDao


  override fun getPushNotificationToken(): Flow<PushTokenEntity> {
    return dao.getPushNotificationToken()
  }

  override suspend fun insertPushNotificationToken(token: PushTokenEntity) {
    dao.insertPushNotificationToken(token)
  }

  override suspend fun clearToken() {
    dao.clearToken()
  }

  override suspend fun sendWaterReminderPushNotification(notification: NotificationDTO) {
    pushNotificationApi.sendWaterReminderPushNotification(notification)
  }
}