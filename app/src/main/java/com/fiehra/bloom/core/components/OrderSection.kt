package com.fiehra.bloom.core.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.WaterDrop
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.fiehra.bloom.core.util.AscDesc
import com.fiehra.bloom.core.util.SortBy

@Composable
fun OrderSection(
  modifier: Modifier = Modifier,
  sortBy: SortBy = SortBy.Name(AscDesc.Ascending),
  onOrderChange: (SortBy) -> Unit
) {

  var orderedBy by remember { mutableStateOf("name") }

  Column(modifier = modifier) {
    Row(
      modifier = Modifier.fillMaxWidth(),
      horizontalArrangement = Arrangement.SpaceBetween
    ) {
      IconButton(
        onClick = { onOrderChange(sortBy.copy(AscDesc.Ascending)) }
      ) {
        if (sortBy.sortBy == AscDesc.Ascending) {
          Icon(
            imageVector = Icons.Filled.ExpandLess,
            contentDescription = "ascending",
            tint = MaterialTheme.colors.primary
          )
        } else {
          Icon(
            imageVector = Icons.Filled.ExpandLess,
            contentDescription = "ascending",
            tint = MaterialTheme.colors.onBackground
          )
        }
      }
      IconButton(
        onClick = { onOrderChange(sortBy.copy(AscDesc.Descending)) }
      ) {
        if (sortBy.sortBy == AscDesc.Descending) {
          Icon(
            imageVector = Icons.Filled.ExpandMore,
            contentDescription = "descending",
            tint = MaterialTheme.colors.primary
          )
        } else {
          Icon(
            imageVector = Icons.Filled.ExpandMore,
            contentDescription = "descending",
            tint = MaterialTheme.colors.onBackground
          )
        }
      }
      IconButton(
        onClick = {
          onOrderChange(SortBy.Name(sortBy.sortBy))
          orderedBy = "name"
        },
      ) {
        if (orderedBy === "name") {
          Icon(
            imageVector = Icons.Filled.Title,
            contentDescription = "name",
            tint = MaterialTheme.colors.primary
          )

        } else {
          Icon(
            imageVector = Icons.Filled.Title,
            contentDescription = "name",
            tint = MaterialTheme.colors.onBackground
          )
        }
      }
      IconButton(
        onClick = {
          onOrderChange(SortBy.WateringFrequency(sortBy.sortBy))
          orderedBy = "watering"
        },
      ) {
        if (orderedBy === "watering") {
          Icon(
            imageVector = Icons.Outlined.WaterDrop,
            contentDescription = "watering",
            tint = MaterialTheme.colors.primary
          )

        } else {
          Icon(
            imageVector = Icons.Outlined.WaterDrop,
            contentDescription = "watering",
            tint = MaterialTheme.colors.onBackground
          )
        }
      }
      IconButton(
        onClick = {
          onOrderChange(SortBy.Light(sortBy.sortBy))
          orderedBy = "light"
        },
      ) {
        if (orderedBy === "light") {
          Icon(
            imageVector = Icons.Filled.WbSunny,
            contentDescription = "light",
            tint = MaterialTheme.colors.primary
          )

        } else {
          Icon(
            imageVector = Icons.Filled.WbSunny,
            contentDescription = "light",
            tint = MaterialTheme.colors.onBackground
          )
        }
      }
    }
  }
}