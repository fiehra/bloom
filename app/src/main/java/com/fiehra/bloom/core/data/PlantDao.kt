package com.fiehra.bloom.core.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fiehra.bloom.core.entities.PlantEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PlantDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insertPlants(
    plantEntities: List<PlantEntity>
  )

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insertPlant(
    plantEntity: PlantEntity
  )

  @Query("DELETE FROM PlantEntity")
  suspend fun clearPlants()

  @Delete
  suspend fun deletePlant(plant: PlantEntity)

  @Query(
    """
    SELECT * 
    FROM Plantentity
    WHERE LOWER(name) LIKE '%' || LOWER(:query)
  """
  )
  fun searchPlants(query: String): Flow<List<PlantEntity>>

  @Query("SELECT * FROM Plantentity WHERE id = :id")
  suspend fun getPlantById(id: String): PlantEntity
}