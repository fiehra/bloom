package com.fiehra.bloom.core.states

import com.fiehra.bloom.core.util.AscDesc
import com.fiehra.bloom.core.util.SortBy
import com.fiehra.bloom.modules.plants.domain.Plant

data class BooleanState(
  val value: Boolean = false,
) {
}

data class PlantNumberState(
  val number: Long = 0,
  val hint: String = "",
) {
}

data class SelectState(
  val text: String = "",
  val hint: String = "",
  val isHintVisible: Boolean = true
) {
}

data class TextfieldState(
  val text: String = "",
  val hint: String = "",
  val isHintVisible: Boolean = true
) {
}

data class MyPlantsState(
  val plants: List<Plant> = emptyList(),
  val sortPlantsBy: SortBy = SortBy.Name(AscDesc.Descending),
  val isOrderSectionVisible: Boolean = false
)

//data class SignupState(
//  val email:String = "",
//  val password:String = "",
//  val passwordRepeated:String = "",
//  val isInputValid:Boolean = false,
//  val isPasswordShown:Boolean = false,
//  val isPasswordRepeatedShown:Boolean = false,
//  val errorMessageInput:String? = null,
//  val isLoading:Boolean = false,
//)

