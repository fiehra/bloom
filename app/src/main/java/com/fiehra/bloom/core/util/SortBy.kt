package com.fiehra.bloom.core.util

sealed class SortBy(val sortBy: AscDesc) {
  class Name(sortBy: AscDesc) : SortBy(sortBy)
  class WateringFrequency(sortBy: AscDesc) : SortBy(sortBy)
  class Light(sortBy: AscDesc) : SortBy(sortBy)

  fun copy(sortBy: AscDesc): SortBy {
    return when (this) {
      is Name -> Name(sortBy)
      is WateringFrequency -> WateringFrequency(sortBy)
      is Light -> Light(sortBy)
    }
  }
}

sealed class AscDesc {
  object Ascending : AscDesc();
  object Descending : AscDesc();
}


