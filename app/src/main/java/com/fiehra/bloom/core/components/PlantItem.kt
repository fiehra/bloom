package com.fiehra.bloom.core.components

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.outlined.WaterDrop
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.fiehra.bloom.modules.plants.domain.Plant

@Composable
fun PlantItem(
  plant: Plant,
  modifier: Modifier = Modifier,
  cornerRadius: Dp = 10.dp,
  onDeleteClick: () -> Unit
) {
  Box(
    modifier = modifier
  ) {
    Canvas(
      modifier = Modifier
        .matchParentSize()
    ) {
      drawRoundRect(
        color = Color(0xFF333333),
        size = size,
        cornerRadius = CornerRadius(cornerRadius.toPx())
      )
    }
    Column(
      modifier = Modifier
        .fillMaxSize()
        .padding(12.dp)
        .padding(end = 32.dp)
    ) {
      Text(
        text = plant.name,
        style = MaterialTheme.typography.h6,
        color = MaterialTheme.colors.primary,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
      )
      Spacer(modifier = Modifier.height(8.dp))
      Text(
        text = plant.wateringFrequency,
        style = MaterialTheme.typography.body1,
        color = MaterialTheme.colors.onSurface,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
      )
      Spacer(modifier = Modifier.height(8.dp))
      Text(
        text = plant.lastWater,
        style = MaterialTheme.typography.body1,
        color = MaterialTheme.colors.onSurface,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
      )
    }
    IconButton(
      onClick = onDeleteClick,
      modifier = Modifier.align(Alignment.BottomEnd),
    ) {
      Icon(
        imageVector = Icons.Default.Delete,
        contentDescription = "delete plant",
        tint = MaterialTheme.colors.primary
      )
    }
    if (plant.thirsty) {
      Icon(
        modifier = Modifier.align(Alignment.TopEnd).padding(
          12.dp
        ),
        imageVector = Icons.Outlined.WaterDrop,
        contentDescription = "thirsty",
        tint = MaterialTheme.colors.secondary
      )
    }
  }

}