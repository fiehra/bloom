package com.fiehra.bloom.core.data

import kotlinx.serialization.Serializable

@Serializable
data class NotificationDTO(
  val plantId: String,
  val registrationToken: String,
  val notificationTitle: String,
  val notificationBody: String,
  val wateringFrequency: String,
  val lastWater: String
)
