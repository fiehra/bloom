package com.fiehra.bloom.core.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun PlantTextfield(
  text: String,
  hint: String,
  modifier: Modifier = Modifier,
  isHintVisible: Boolean = true,
  onValueChange: (String) -> Unit,
  singleLine: Boolean = false,
  onFocusChange: (FocusState) -> Unit,
) {
  Box(
    modifier = modifier
  ) {
    TextField(
      value = text,
      onValueChange = onValueChange,
      singleLine = singleLine,
      textStyle = TextStyle(color = MaterialTheme.colors.primary, fontSize = 22.sp),
      modifier = Modifier
        .fillMaxWidth()
        .onFocusChanged {
          onFocusChange(it)
        },
    )
    if (isHintVisible) {
      Text(
        text = hint,
        modifier = Modifier.padding(15.dp),
      )
    }
  }
}