package com.fiehra.bloom.di

import android.app.Application
import androidx.room.Room
import com.fiehra.bloom.core.data.PlantDatabase
import com.fiehra.bloom.modules.authentication.domain.AuthRepositoryI
import com.fiehra.bloom.modules.authentication.domain.AuthUseCases
import com.fiehra.bloom.modules.authentication.domain.Login
import com.fiehra.bloom.modules.authentication.domain.Signup
import com.fiehra.bloom.modules.authentication.domain.ValidateRegisterInputUseCase
import com.fiehra.bloom.modules.notifications.AddToken
import com.fiehra.bloom.modules.notifications.GetToken
import com.fiehra.bloom.modules.notifications.NotificationRepositoryI
import com.fiehra.bloom.modules.notifications.NotificationUseCases
import com.fiehra.bloom.modules.notifications.SendPush
import com.fiehra.bloom.modules.plants.domain.AddPlant
import com.fiehra.bloom.modules.plants.domain.DeletePlant
import com.fiehra.bloom.modules.plants.domain.GetPlants
import com.fiehra.bloom.modules.plants.domain.PlantUseCases
import com.fiehra.bloom.modules.plants.domain.UpdatePlant
import com.fiehra.bloom.modules.plants.domain.PlantRepositoryI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
  @Provides
  @Singleton
  fun providePlantDatabase(app: Application): PlantDatabase {
    return Room.databaseBuilder(
      app,
      PlantDatabase::class.java,
      "plants.db"
    ).build()
  }

  @Provides
  @Singleton
  fun providePlantUseCases(repository: PlantRepositoryI): PlantUseCases {
    return PlantUseCases(
      getPlants = GetPlants(repository),
      addPlant = AddPlant(repository),
      deletePlant = DeletePlant(repository),
      updatePlant = UpdatePlant(repository)
    )
  }

  @Provides
  @Singleton
  fun provideAuthUseCases(): AuthUseCases {
    return AuthUseCases(
      signup = Signup(),
      login = Login(),
      validateRegisterInputUseCase = ValidateRegisterInputUseCase(),
    )
  }

  @Provides
  @Singleton
  fun provideNotificationUseCases(repository: NotificationRepositoryI): NotificationUseCases {
    return NotificationUseCases(
      addToken = AddToken(repository),
      getToken = GetToken(repository),
      sendPush = SendPush(repository),
    )
  }
}