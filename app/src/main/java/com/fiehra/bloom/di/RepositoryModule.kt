package com.fiehra.bloom.di

import com.fiehra.bloom.core.data.PlantRepository
import com.fiehra.bloom.modules.plants.domain.PlantRepositoryI
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
  @Binds
  @Singleton
  abstract fun bindPlantRepository(
    plantRepository: PlantRepository
  ): PlantRepositoryI
}