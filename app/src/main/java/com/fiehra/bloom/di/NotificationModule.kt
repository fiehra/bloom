package com.fiehra.bloom.di

import com.fiehra.bloom.core.data.NotificationRepository
import com.fiehra.bloom.modules.notifications.NotificationRepositoryI
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NotificationModule {
  @Binds
  @Singleton
  abstract fun bindNotificationRepository(
    notificationRepository: NotificationRepository
  ): NotificationRepositoryI
}