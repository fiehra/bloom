package com.fiehra.bloom

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.Modifier
import androidx.navigation.navArgument
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.fiehra.bloom.core.util.Screen
import com.fiehra.bloom.modules.plants.ui.add_edit_plant.AddEditPlantScreen
import com.fiehra.bloom.modules.plants.ui.my_plants.MyPlantsScreen
import com.fiehra.bloom.modules.plants.ui.my_plants.SignupScreen
import dagger.hilt.android.AndroidEntryPoint
import com.fiehra.bloom.ui.theme.BloomTheme


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      BloomTheme() {
        // A surface container using the 'background' color from the theme
        Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
          val navController = rememberNavController()
          NavHost(
            navController = navController,
            startDestination = Screen.SignupScreen.route
          ) {
            composable(route = Screen.MyPlantsScreen.route) {
              MyPlantsScreen(
                navController = navController,
              )
            }
            composable(route = Screen.SignupScreen.route) {
              SignupScreen(
                navController = navController,
              )
            }
            composable(
              route = Screen.AddEditPlantScreen.route + "?plantId={plantId}",
              arguments = listOf(
                navArgument(name = "plantId") {
                  type = NavType.StringType
                  defaultValue = ""
                },
              )
            ) {
              AddEditPlantScreen(
                navController = navController,
              )
            }
          }
        }
      }
    }
  }
}
